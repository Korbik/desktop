# Copyright 2009 Maxime Coste <frrrwww@gmail.com>
# Copyright 2014 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require wxGTK

PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    gstreamer
    joystick
    libnotify
    sdl
    tiff
    webkit

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/expat
        dev-libs/glib:2
        media-libs/libpng:=
        x11-dri/glu
        x11-dri/mesa
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXpm
        x11-libs/libXxf86vm
        x11-libs/pango [[ note = [ required for unicode support ] ]]
        gstreamer? (
            media-libs/gstreamer:1.0
            media-plugins/gst-plugins-base:1.0
        )
        libnotify? ( x11-libs/libnotify[>=0.7] )
        providers:gtk2? ( x11-libs/gtk+:2[>=2.24] )
        providers:gtk3? ( x11-libs/gtk+:3[>=3.10.2] )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        sdl? ( media-libs/SDL:2 )
        tiff? ( media-libs/tiff )
        webkit? ( net-libs/webkit:4.0[>=1.3.1] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Avoid-collisions.patch
    "${FILES}"/0001-Fix-pkg-config-invocation.patch
)

configure_one_multibuild() {
    econf \
        PKG_CONFIG="/usr/$(exhost --target)/bin/$(exhost --tool-prefix)pkg-config" \
        --hates=docdir \
        --disable-compat26 \
        --disable-gtktest \
        --enable-compat28 \
        --enable-controls \
        --enable-expat=sys \
        --enable-graphics_ctx \
        --enable-gui \
        --enable-intl \
        --enable-ipv6 \
        --enable-shared \
        --enable-threads \
        --enable-unicode \
        $(if exhost --is-native -q;then
            echo --enable-precomp-headers
        else
            echo --disable-precomp-headers
        fi) \
        --with-gtk=${GTK_VERSION} \
        --with-gtkprint \
        --with-libjpeg=sys \
        --with-libpng=sys \
        --with-libxpm=sys \
        --with-libiconv \
        --with-opengl \
        --with-regex=builtin \
        --with-zlib=sys \
        --without-gnomevfs \
        --without-motif \
        --without-wine \
        $(option_enable joystick) \
        $(option_enable gstreamer mediactrl) \
        $(option_enable webkit webviewwebkit) \
        $(option_with libnotify) \
        $(option_with sdl) \
        $(option_with tiff libtiff sys)
}

